# Reddit Scraper

My first attempt at using praw (Python Reddit API Wrapper) to scrape reddit.com.

## What does it do?

This scraper is intended to access your saved posts and download any posts with a file ending of jpg, gif, png or jpeg.  
It then proceeds to unsave the post to make it easier for the user


## Setup

Run setup.py to create all necessary files.  

After this is done head over into the setup.txt file that was created and set it up with the format.  

```
client_id=
client_secret=
user_agent=
username=
password=
```  

Look at the official [PRAW documentation](https://praw.readthedocs.io/en/latest/getting_started/quick_start.html#obtain-a-reddit-instance) on what to write.

## Scrape user saved posts

#### Unsave

If you want to unsave the files it downloads, you will have to navigate to the getLinks() function and remove the comment.  

```
# submission.unsave() -> submission.unsave()
```

#### Subreddits

Currently there is a few default subreddits that i use myself.  
You can add more by navigation into the file and add your wanted subreddit into the list "AllowedSubreddit"  
Get your subreddits by navigating to reddit and then go into the subreddit and take the last part after /r/.  
For example 

```
https://www.reddit.com/r/BirdsForScale -> BirdsForScale
```

#### Default subreddits
```
allowedSubreddit = ['Animewallpaper', 'KemonoFriends',
                    'ZeroTwo', 'kitsunemimi', 'awoo', 'Vocaloid']
```

## Scrape subreddits

Scraping subreddits is simple navigate to the scrapeSubreddit.py change the scrapeSubreddit value.

```
scrapeSubreddit = 'kemonofriends' -> scrapeSubreddit = 'BirdsForScale'
```

Make sure you only choose one subreddit at a time otherwise it will not work.

## Debugging

### 401 HTTP response - UNAUTHORIZED

Getting a [401](https://httpstatuses.com/401) error, means the request you have sent does not have valid authentication credentials.  

Make sure you have the correct information in setup.txt and try again.

### invalid_grant error processing request

Make sure your setup.txt has the same format as mentioned [above](https://gitlab.com/lividcomet/reddit-scraper#first-time-setup).  

This can occur if you for example change the order of things or type the wrong information.
