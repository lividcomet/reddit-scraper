#! usr/bin/env python3


import requests
import urllib.request
import os
from Setup import checkFiles, redditSetup


suffixList = ['jpg', 'gif', 'png', 'jpeg']
allowedSubreddit = ['Animewallpaper', 'KemonoFriends',
                    'ZeroTwo', 'kitsunemimi', 'awoo', 'Vocaloid']
filePath = os.path.abspath('./scrapePic')


def save(url, submission):
    if submission.over_18 == True:
        urllib.request.urlretrieve(
            url, filePath + '/' + 'nsfw' + url.split('/')[-1])
    else:
        urllib.request.urlretrieve(
            url, filePath + '/' + url.split('/')[-1])


def getLinks(reddit):
    x = 0
    seq = reddit.user.me().saved(limit=None)

    for submission in seq:
        x += 1
        print(x)
        if submission.url.split('.')[-1] not in suffixList:
            continue
        save(submission.url, submission)
        # submission.unsave()


checkFiles()
getLinks(redditSetup())
