#! usr/bin/env python3

import sys
import os
import praw

filePath = os.path.abspath('./scrapePic')
filePath2 = os.path.abspath('setup.txt')


def checkFiles():
    # checkFiles makes sure that the necessary files exist and if they do not, it creates them.
    if not os.path.isdir(filePath):
        os.makedirs(filePath)
    if not os.path.isfile(filePath2):
        setupFile = open(filePath2, 'w+')
        setupFile.close()
        print("Please setup the file 'setup.txt' and then run the program again.")
        sys.exit()


def redditSetup():
    # redditSetup reads from the setup.txt and sets up praw.
    setup = []
    x = 0
    with open("setup.txt") as f:
        content = f.readlines()
    content = [x.strip() for x in content]
    for line in content:
        split = content[x].split('=').pop(1)
        setup.append(split)
        x += 1
    reddit = praw.Reddit(client_id=str(setup[0]), client_secret=str(setup[1]),
                         user_agent=str(setup[2]), username=str(setup[3]), password=str(setup[4]))
    return reddit
