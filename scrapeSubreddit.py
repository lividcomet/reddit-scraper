#! usr/bin/env python3

import requests
import urllib.request
import os
from Setup import checkFiles, redditSetup


suffixList = ['jpg', 'gif', 'png', 'jpeg']
filePath = os.path.abspath('./scrapePic')


def save(url, submission):
    if submission.over_18 == True:
        urllib.request.urlretrieve(
            url, filePath + '/' + 'nsfw' + url.split('/')[-1])
    else:
        urllib.request.urlretrieve(
            url, filePath + '/' + url.split('/')[-1])


def getSubredditLinks(reddit):
    # getSubredditLinks goes through the set limit amount of "Hot" posts,
    #  and saves them into scrape pic
    x = 0
    seq = reddit.subreddit('kemonofriends').hot(limit=10)

    for submission in seq:
        x += 1
        print(x)
        if submission.url.split('.')[-1] not in suffixList:
            continue
        save(submission.url, submission)


checkFiles()
getSubredditLinks(redditSetup())
